package view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import model.domain.Boat;
import model.domain.Member;

/**
 * Class for communicating with the console in English.
 */
public class EnglishUi implements ConsoleUi {
  private Scanner scan = new Scanner(System.in, "UTF-8");

  public void greeting() {
    System.out.println("\n******** Welcome ********");
  }

  /**
   * Prints the main menu.
   *
   * @return - A string of the user's choice.
   */
  public MenuAction printMenu() {
    System.out.println("\nPlease choose one of the following:(a-c)");
    System.out.println("a) Show a verbose list of all members.");
    System.out.println("b) Show a compact list of all members.");
    System.out.println("c) Show member options.");
    System.out.println("q) Quit.");

    String choice = scan.nextLine();
    
    if (choice.equals("a")) {
      return MenuAction.VerboseList;
    } else if (choice.equals("b")) {
      return MenuAction.CompactList;
    } else if (choice.equals("c")) {
      return MenuAction.MemberOptions;
    }

    return MenuAction.Quit;
  }

  /**
   * Prints a list of some data of the members.
   *
   * @param members - A list of members.
   */
  public void printVerboseList(ArrayList<Member> members) {
    Collections.sort(members, Comparator.comparing(Member::getName));

    for (Member member : members) {
      printMemberData(member);
    }
  }

  /**
   * Print a list of all the data of the members. 
   *
   * @param members - A list of member objects.
   */
  public void printCompactList(ArrayList<Member> members) {
    Collections.sort(members, Comparator.comparing(Member::getName));

    for (Member m : members) {
      System.out.println("\nName: " + m.getName() + "\nID: " + m.getId() + "\nNumber of boats: " + m.allBoats().size());
    }
  }

  /**
   * Prints the member options menu. 
   *
   * @return - The user's choice.
   */
  public CrudAction showMemberOptions() {
    System.out.println("\nPlease choose one of the following:(a-d)");
    System.out.println("a) Add new member. \nb) Show existing member.");
    System.out.println("c) Update existing member. \nd) Delete member.");

    return handleRestOptions();
  }

  /**
   * Returns the user's choice.

   * @return - The user's choice.
   */
  private CrudAction handleRestOptions() {
    String choice = scan.nextLine();
    if (choice.equals("a")) {
      return CrudAction.Add;
    } else if (choice.equals("b")) {
      return CrudAction.Show;
    } else if (choice.equals("c")) {
      return CrudAction.Update;
    } else if (choice.equals("d")) {
      return CrudAction.Delete;
    }

    return CrudAction.Quit;
  }

  /**
   * Prints the options for updating a member.
   *
   * @return - The user's choice.
   */
  public UpdateMemberAction updateMemberOptions() {
    System.out.println("\nWhat would you like to do?(a-d)");
    System.out.println("\na) Change Name \nb) Change Personal number \nc) Change Boat's information");
    System.out.println("\nd) Add Boat \ne) Delete a Boat");

    return handleMemberAction();
  }

  /**
   * Returns the user's choice from the member menu.

   * @return - The user's choice.
   */
  private UpdateMemberAction handleMemberAction() {
    String choice = scan.nextLine();

    if (choice.equals("a")) {
      return UpdateMemberAction.Name;
    } else if (choice.equals("b")) {
      return UpdateMemberAction.PerNumber;
    } else if (choice.equals("c")) {
      return UpdateMemberAction.ChangeBoat;
    } else if (choice.equals("d")) {
      return UpdateMemberAction.AddBoat;
    } else if (choice.equals("e")) {
      return UpdateMemberAction.DeleteBoat;
    }

    return UpdateMemberAction.Quit;
  }

  /**
   * Prints the options for updating a boat. 
   *
   * @return - The user's choice.
   */
  public UpdateBoatAction updateBoatOptions() {
    System.out.println("\nWhat information about the boat would you like to change?(a-b)");
    System.out.println("\na) Name \nb) Type \nc) Length");

    String choice = scan.nextLine();
    if (choice.equals("a")) {
      return UpdateBoatAction.Name;
    } else if (choice.equals("b")) {
      return UpdateBoatAction.Type;
    } else if (choice.equals("c")) {
      return UpdateBoatAction.Length;
    }

    return UpdateBoatAction.Quit;
  }

  /**
   * Prompts the user for a boat name and capitalizes it.

   * @return - The boat name.
   */
  public String promptBoatName() {
    System.out.println("\nWhat is the name of the boat?");
    String name = scan.nextLine();
    String capitalized = name.substring(0, 1).toUpperCase() + name.substring(1);
    return capitalized;
  }

  public void boatNameNotUnique() {
    System.out.println("\nThat name is not unique");
  }

  public String promptMemberName() {
    System.out.println("\nWhat is the full name of the member?");
    return scan.nextLine();
  }

  public String promptPersonalNum() {
    System.out.println("\nWhat is the personal number of the new member?");
    return scan.nextLine();
  }

  public String promptBoatOwner() {
    System.out.println("\nEnter the ID of the owner of the boat you would like to register?");
    return scan.nextLine();
  }

  public String promptBoatType() {
    System.out.println("\nWhat type of boat would you like to register?");
    return scan.nextLine();
  }

  public String promptBoatLength() {
    System.out.println("\nHow long(meters) is the boat you would like to register?");
    return scan.nextLine();
  }

  public String promptIdSearch() {
    System.out.println("\nPlease enter the ID of the member you wish to search for.");
    return scan.nextLine();
  }

  /**
   * Asks the user which boat they would like to update.
   *
   * @param boats - A list of boat objects.
   * @return - A boat.
   */
  public Boat promptWhichBoat(ArrayList<Boat> boats) {
    System.out.println("\nWhich of the following boats would you like to choose?(1 - " + boats.size() + " )");
    for (Boat boat : boats) {
      System.out.println((boats.indexOf(boat) + 1) + ". " + boat.getName());
      System.out.println(boat.getType() + " " + boat.getLength() + " meters");
    }

    Integer choice = Integer.parseInt(scan.nextLine());

    for (Boat boat : boats) {
      if ((boats.indexOf(boat) + 1) == choice) {
        return boat;
      }
    }
    return null;
  }

  /**
   * Print all data about a member.
   *
   * @param member - A member object.
   */
  public void printMemberData(Member member) {
    StringBuffer boatString = new StringBuffer();
    for (Boat boat : member.allBoats()) {
      boatString.append("\n* " + boat.getName() + " " + boat.getType() + " " + boat.getLength());
    }
    System.out.println("\nName: " + member.getName() + "\nPersonal Number: " + member.getPersonalNum());
    System.out.println("ID: " + member.getId() + "\nBoats: " + boatString);
  }

  /**
   * Prints a member's boats.

   * @param boats - A list of Boat objects.
   */
  public void printBoats(ArrayList<Boat> boats) {
    if (boats != null) {
      for (Boat boat : boats) {
        System.out.println("\nBoats: \nName: " + boat.getName());
        System.out.println("\nType: " + boat.getType() + "\nLength: " + boat.getLength() + " meters");
      }
    } else {
      printBoatNotFound();
    }
  }

  public void createdMemberMessage() {
    System.out.println("The member was successfully created!");
  }

  public void createdBoatMessage() {
    System.out.println("The boat was successfully created and added to member!");
  }

  public void updateMessage() {
    System.out.println("Successfully updated!");
  }

  public void deleteMessage() {
    System.out.println("Successfully deleted from the registry!");
  }

  public void printMemberNotFound() {
    System.out.println("Sorry, but the member you searched for couldn't be found in our registry. ");
  }

  public void printBoatNotFound() {
    System.out.println("Sorry, but we couldn't find any boats registered to that member.");
  }

  public void printErrorMessage() {
    System.out.println("There was a problem with your input.");
  }
}
