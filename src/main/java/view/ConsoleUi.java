package view;

import java.util.ArrayList;
import model.domain.Boat;
import model.domain.Member;

/**
 * The interface for the console output.
 */
public interface ConsoleUi {
  /**
   * Main menu action.
   */
  public enum MenuAction {
    VerboseList, 
    CompactList,
    MemberOptions,
    Quit
  }

  void greeting();
  
  /**
   * Prints the main menu.
   *
   * @return - A string of the user's choice.
   */
  MenuAction printMenu();
  
  /**
   * Prints a list of some data of the members.
   *
   * @param members - A list of members.
   */
  void printVerboseList(ArrayList<Member> members);
  
  /**
   * Print a list of all the data of the members. 
   *
   * @param members - A list of member objects.
   */
  void printCompactList(ArrayList<Member> members);

  /**
   * CRUD actions for persistent data.
   */
  public enum CrudAction {
    Add,
    Show,
    Update,
    Delete,
    Quit
  }
  
  /**
   * Prints the member options menu. 
   *
   * @return - The user's choice.
   */
  CrudAction showMemberOptions();

  /**
   * Actions for updating a member.
   */
  public enum UpdateMemberAction {
    Name,
    PerNumber,
    ChangeBoat,
    AddBoat,
    DeleteBoat,
    Quit
  }

  /**
   * Actions for updating a boat.
   */
  public enum UpdateBoatAction {
    Name,
    Type,
    Length,
    Quit
  }
  
  /**
   * Prints the options for updating a member.
   *
   * @return - The user's choice.
   */
  UpdateMemberAction updateMemberOptions();
  
  /**
   * Prints the options for updating a boat. 
   *
   * @return - The user's choice.
   */
  UpdateBoatAction updateBoatOptions();
  
  String promptBoatName();

  void boatNameNotUnique();
  
  String promptMemberName();
  
  String promptPersonalNum();
  
  String promptBoatOwner();
  
  String promptBoatType();
  
  String promptBoatLength();
  
  String promptIdSearch();
  
  /**
   * Asks the user which boat they would like to update.
   *
   * @param boats - A list of boat objects.
   * @return - A boat.
   */
  Boat promptWhichBoat(ArrayList<Boat> boats);
  
  /**
   * Print all data about a member.
   *
   * @param member - A member object.
   */
  void printMemberData(Member member);
  
  /**
   * Prints a member's boats.

   * @param boats - A list of Boat objects.
   */
  void printBoats(ArrayList<Boat> boats);
  
  void createdMemberMessage();
  
  void createdBoatMessage();
  
  void updateMessage();
  
  void deleteMessage();
  
  void printMemberNotFound();
  
  void printBoatNotFound();
  
  void printErrorMessage();
}
