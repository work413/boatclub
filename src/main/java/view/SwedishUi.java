package view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import model.domain.Boat;
import model.domain.Member;

/**
 * Class for communicating with the console in Swedish.
 */
public class SwedishUi implements ConsoleUi {
  private Scanner scan = new Scanner(System.in, "UTF-8");

  public void greeting() {
    System.out.println("\n******** Välkommen ********");
  }
    
  /**
   * Prints the main menu.
   *
   * @return - A string of the user's choice.
   */
  public MenuAction printMenu() {
    System.out.println("\nVälj ett av följande alternativ i listan:(1-3)");
    System.out.println("1) Visa en detaljerad lista av alla medlemmar.");
    System.out.println("2) Visa en kompakt lista av alla medlemmar.");
    System.out.println("3) Visa alternativ för medlemmar.");
    System.out.println("q) Avsluta");
    
    String choice = scan.nextLine();
        
    if (choice.equals("1")) {
      return MenuAction.VerboseList;
    } else if (choice.equals("2")) {
      return MenuAction.CompactList;
    } else if (choice.equals("3")) {
      return MenuAction.MemberOptions;
    }
    
    return MenuAction.Quit;
  }
    
  /**
   * Prints a list of some data of the members.
   *
   * @param members - A list of members.
   */
  public void printVerboseList(ArrayList<Member> members) {
    Collections.sort(members, Comparator.comparing(Member::getId));
    
    for (Member member : members) {
      printMemberData(member);
    }
  }
    
  /**
   * Print a list of all the data of the members. 
   *
   * @param members - A list of member objects.
   */
  public void printCompactList(ArrayList<Member> members) {
    Collections.sort(members, Comparator.comparing(Member::getId));

    for (Member m : members) {
      System.out.println("\nID: " + m.getId() + "\nNamn: " + m.getName() + "\nAntal båtar: " + m.allBoats().size());
    }
  }
    
  /**
   * Prints the member options menu. 
   *
   * @return - The user's choice.
   */
  public CrudAction showMemberOptions() {
    System.out.println("\nVälj ett av följande alternativ:(1-4)");
    System.out.println("1) Lägg till en ny medlem. \n2) Visa en medlem.");
    System.out.println("3) Ändra information om en medlem. \n4) Radera en medlem.");
    
    return handleRestOptions();
  }
    
  /**
   * Returns the user's choice.

   * @return - The user's choice.
   */
  private CrudAction handleRestOptions() {
    String choice = scan.nextLine();
    if (choice.equals("1")) {
      return CrudAction.Add;
    } else if (choice.equals("2")) {
      return CrudAction.Show;
    } else if (choice.equals("3")) {
      return CrudAction.Update;
    } else if (choice.equals("4")) {
      return CrudAction.Delete;
    }
    
    return CrudAction.Quit;
  }
    
  /**
   * Prints the options for updating a member.
   *
   * @return - The user's choice.
   */
  public UpdateMemberAction updateMemberOptions() {
    System.out.println("\nVad vill du göra?(1-4)");
    System.out.println("\n1) Ändra namn \n2) Ändra personnummer \n3) Ändra en båts information");
    System.out.println("\n4) Lägg till en båt \n5) Radera en båt");
    
    return handleMemberAction();
  }

  /**
   * Returns the user's choice from the member menu.
   *
   * @return - The user's choice.
   */
  private UpdateMemberAction handleMemberAction() {
    String choice = scan.nextLine();
    
    if (choice.equals("1")) {
      return UpdateMemberAction.Name;
    } else if (choice.equals("2")) {
      return UpdateMemberAction.PerNumber;
    } else if (choice.equals("3")) {
      return UpdateMemberAction.ChangeBoat;
    } else if (choice.equals("4")) {
      return UpdateMemberAction.AddBoat;
    } else if (choice.equals("5")) {
      return UpdateMemberAction.DeleteBoat;
    }
    
    return UpdateMemberAction.Quit;
  }
    
  /**
   * Prints the options for updating a boat. 

   * @return - The user's choice.
   */
  public UpdateBoatAction updateBoatOptions() {
    System.out.println("\nVilken information om båten skulle du vilja ändra?(1-3)");
    System.out.println("\n1) Namn \n2) Typ \n3) Längd");
    
    String choice = scan.nextLine();

    if (choice.equals("1")) {
      return UpdateBoatAction.Name;
    } else if (choice.equals("2")) {
      return UpdateBoatAction.Type;
    } else if (choice.equals("3")) {
      return UpdateBoatAction.Length;
    } else {
      return UpdateBoatAction.Quit;
    }
  }

  /**
   * Prompts the user for a boat name and capitalizes it.

   * @return - The boat name.
   */
  public String promptBoatName() {
    System.out.println("\nVad är båtens namn?");
    String name = scan.nextLine();
    String capitalized = name.substring(0, 1).toUpperCase() + name.substring(1);
    return capitalized;
  }
    
  public void boatNameNotUnique() {
    System.out.println("\nNamnet måste vara unikt.");
  }
    
  public String promptMemberName() {
    System.out.println("\nVad heter medlemmen?");
    return scan.nextLine();
  }
    
  public String promptPersonalNum() {
    System.out.println("\nVad är personnumret av medlemmen?");
    return scan.nextLine();
  }
    
  public String promptBoatOwner() {
    System.out.println("\nSkriv ID:et av medlemmen som äger båten?");
    return scan.nextLine();
  }
    
  public String promptBoatType() {
    System.out.println("\nAv vilken typ är båten?");
    return scan.nextLine();
  }
    
  public String promptBoatLength() {
    System.out.println("\nHur lång är båten(meter)?");
    return scan.nextLine();
  }
    
  public String promptIdSearch() {
    System.out.println("\nSkriv ID:et av medlemmen du vill söka efter.");
    return scan.nextLine();
  }
    
  /**
   * Asks the user which boat they would like to update.

   * @param boats - A list of boat objects.
   * @return - A boat.
   */
  public Boat promptWhichBoat(ArrayList<Boat> boats) {
    System.out.println("\nVilken av följande båtar vill du välja?");
    System.out.println("1 - " + boats.size());

    for (Boat boat : boats) {
      System.out.println((boats.indexOf(boat) + 1) + ". ");
      System.out.println(boat.getName() + " " + boat.getType() + " " + boat.getLength() + " meter");
    }
    
    Integer choice = Integer.parseInt(scan.nextLine());
    
    for (Boat boat : boats) {
      if ((boats.indexOf(boat) + 1) == choice) {
        return boat;
      }
    }
    
    return null;
  }
    
  /**
   * Print all data about a member.
   *
   * @param member - A member object.
   */
  public void printMemberData(Member member) {
    StringBuffer boatString = new StringBuffer();
    for (Boat boat : member.allBoats()) {
      boatString.append("\n* " + boat.getName() + " " + boat.getType() + " " + boat.getLength());
    }
    System.out.println("\nID: " + member.getId() + "\nNamn: " + member.getName());
    System.out.println("Personnummer: " + member.getPersonalNum() + "\nBåtar: " + boatString);
  }

  /**
   * Prints a member's boats.

   * @param boats - A list of Boat objects.
   */
  public void printBoats(ArrayList<Boat> boats) {
    if (boats != null) {
      for (Boat boat : boats) {
        System.out.println("\nBåtar: \nNamn: " + boat.getName());
        System.out.println("\nTyp: " + boat.getType() + "\nLängd: " + boat.getLength() + " meter");
      }
    } else {
      printBoatNotFound();
    }
  }
    
  public void createdMemberMessage() {
    System.out.println("Medlemmen skapades!");
  }
    
  public void createdBoatMessage() {
    System.out.println("Båten är nu tillagd i medlemmen!");
  }
    
  public void updateMessage() {
    System.out.println("Uppdatering lyckades!");
  }
    
  public void deleteMessage() {
    System.out.println("Raderades!");
  }
    
  public void printMemberNotFound() {
    System.out.println("Medlemmen du sökte efter kunde inte hittas!");
  }
    
  public void printBoatNotFound() {
    System.out.println("Inga båtar kunde hittas tillhörande den medlemmen.");
  }
    
  public void printErrorMessage() {
    System.out.println("Det uppstod ett problem med din indata.");
  }
}
