package controller;

import java.util.ArrayList;
import model.domain.Boat;
import model.domain.Member;
import model.domain.PersistenceManager;
import view.ConsoleUi;

/**
 * Executes the usage scenarios and handles the state of the user interface. 
 */
public class Register {
  private ConsoleUi ui;
  private PersistenceManager manager;
  private boolean quit = false;

  /**
   * Sets an attribute as well as creates a new persistence manager with the members in it.

   * @param members - An array of member objects.
   * @param ui - The user interface.
   */
  public Register(ArrayList<Member> members, ConsoleUi ui) {
    manager = new PersistenceManager();
    manager.addSavedMembers(members);
    this.ui = ui;
  }

  public ArrayList<Member> getMembers() {
    return manager.allMembers();
  }

  /**
   * Handles the user's menu choice. 
   */
  public void handleMenu() {
    do {
      ConsoleUi.MenuAction choice = ui.printMenu();
      switch (choice) {
        case VerboseList:
          ui.printVerboseList(manager.allMembers());
          break;
        case CompactList:
          ui.printCompactList(manager.allMembers());
          break;
        case MemberOptions:
          ConsoleUi.CrudAction memberChoice = ui.showMemberOptions();
          handleMemberOptions(memberChoice);
          break;
        case Quit:
          quit = true;
          return;
        default:
          ui.printErrorMessage();
          break;
      }
    } while (!quit);
  }

  /**
   * Handles the user's choice from the member menu.
   *
   * @param memberChoice - The user's menu choice.
   */
  private void handleMemberOptions(ConsoleUi.CrudAction memberChoice) {
    String idSearch;
    switch (memberChoice) {
      case Add:
        String name = ui.promptMemberName();
        Long personalNum = Long.parseLong(ui.promptPersonalNum());
        manager.createNewMember(name, personalNum);
        ui.createdMemberMessage();
        break;
      case Show:
        String viewSearch = ui.promptIdSearch();
        Member member = manager.getMember(viewSearch);
        if (member != null) {
          ui.printMemberData(member);
        } else {
          ui.printMemberNotFound();
        }
        break;
      case Update:
        idSearch = ui.promptIdSearch();
        member = manager.getMember(idSearch);
        if (member != null) {
          ui.printMemberData(member);
          ConsoleUi.UpdateMemberAction toUpdate = ui.updateMemberOptions();
          if (updateMember(member, toUpdate)) {
            ui.printMemberData(member);
            ui.updateMessage();
          }
        } else {
          ui.printMemberNotFound();
        }
        break;
      case Delete:
        idSearch = ui.promptIdSearch();
        if (manager.deleteMember(idSearch)) {
          ui.deleteMessage();
        } else {
          ui.printMemberNotFound();
        }
        break;
      default:
        ui.printErrorMessage();
        break;
    }
    handleMenu();
  }

  /**
   * Handles the user's choice for updating a member's information.
   *
   * @param member - A Member object.
   * @param toUpdate - The user's update choice. 
   */
  private boolean updateMember(Member member, ConsoleUi.UpdateMemberAction toUpdate) {
    switch (toUpdate) {
      case Name:
        String newName = ui.promptMemberName();
        manager.setMemberName(member, newName);
        break;
      case PerNumber:
        Long personalNum = Long.parseLong(ui.promptPersonalNum());
        manager.setMemberPersonalNum(member, personalNum);
        break;
      case ChangeBoat:
        if (member.allBoats().size() > 0) {
          Boat boatToUpdate = ui.promptWhichBoat(member.allBoats());
          if (boatToUpdate != null) {
            updateBoat(boatToUpdate, member);
          } else {
            ui.printErrorMessage();
            return false;
          }
        } else {
          ui.printBoatNotFound();
          return false;
        }
        break;
      case AddBoat:
        String boatName = ui.promptBoatName();

        if (!manager.uniqueBoatName(boatName)) {
          ui.boatNameNotUnique();
          return false;
        }

        String type = ui.promptBoatType();
        Float length = Float.parseFloat(ui.promptBoatLength());

        Boat newBoat = manager.createNewBoat(boatName, type, length);
        manager.setBoatToMember(member, newBoat);
      
        ui.createdBoatMessage();
        break;
      case DeleteBoat:
        if (member.allBoats().size() > 0) {
          Boat boatToDelete = ui.promptWhichBoat(member.allBoats());
          if (boatToDelete != null) {
            manager.deleteBoat(member, boatToDelete);
            return true;
          } else {
            ui.printErrorMessage();
            return false;
          }
        } else {
          ui.printBoatNotFound();
          return false;
        }
      default:
        ui.printErrorMessage();
        updateMember(member, ui.updateMemberOptions());
        break;
    }
    return true;
  }

  /**
   * Handles the user's choice for updating a boat's information.
   *
   * @param boat - The Boat object to update.
   * @param member - A Member object.
   * @return - A Boat object.
   */
  private Boat updateBoat(Boat boat, Member member) {
    ConsoleUi.UpdateBoatAction infoToUpdate = ui.updateBoatOptions();

    switch (infoToUpdate) {
      case Name:
        String boatName = "";
        do {
          boatName = ui.promptBoatName();
        } while (!manager.uniqueBoatName(boatName));
        manager.setBoatName(boat, boatName);
        return boat;
      case Type:
        String newType = ui.promptBoatType();
        manager.setBoatType(boat, newType);
        return boat;
      case Length:
        Float newLength = Float.parseFloat(ui.promptBoatLength());
        manager.setBoatLength(boat, newLength);
        return boat;
      default:
        ui.printErrorMessage();
        updateBoat(boat, member);
        return boat;
    }
  }
}
