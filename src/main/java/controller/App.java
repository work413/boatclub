package controller;

import java.io.IOException;
import java.util.ArrayList;
import model.domain.Member;
import model.persistence.PersistentData;
import view.ConsoleUi;
import view.EnglishUi;
import view.SwedishUi;

/**
 * Responsible for staring the application.
 */
public class App {
  /**
   * Application starting point.

   * @param args command line arguments.
   * @throws IOException - Throws exception.
   */
  public static void main(String[] args) throws IOException {
    PersistentData persistence = new PersistentData();
    ConsoleUi ui = new SwedishUi(); //3 new EnglishUi(); //
  
    ui.greeting();

    ArrayList<Member> members = persistence.getListFromFile("members.txt");

    Register register = new Register(members, ui);

    register.handleMenu();

    ArrayList<Member> updatedMembers = register.getMembers();

    persistence.writeMembersToFile(updatedMembers);
  }
}
