package model.persistence;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import model.domain.Boat;
import model.domain.Member;

/**
 * Class for handling persistent data.
 */
public class PersistentData {
  /**
   * Generates a list of saved data from file.
   *
   * @param fileLocation - The address to the file.
   * @return - A list of saved data.
   * @throws IOException - Throws exception.
   */
  public ArrayList<Member> getListFromFile(String fileLocation) throws IOException {
    FileInputStream persistentData = new FileInputStream(fileLocation);
    InputStreamReader streamReader = new InputStreamReader(persistentData, "UTF-8");
    BufferedReader reader = new BufferedReader(streamReader);
    ArrayList<String> dataList = getDataList(reader);
    reader.close();

    return memberObjectsList(dataList);
  }

  /**
   * Generates a list of saved data.
   *
   * @param reader - A reader.
   * @return - A list of data.
   * @throws IOException - Throws exception.
   */
  private ArrayList<String> getDataList(BufferedReader reader) throws IOException {
    ArrayList<String> dataList = new ArrayList<>();
    String line;
    while ((line = reader.readLine()) != null) {
      dataList.add(line);
    }
    
    return dataList;
  }

  /**
   * Writes members to persistent file.
   *
   * @param members - A list of member objects.
   * @throws IOException - Throws exception.
   */
  public void writeMembersToFile(ArrayList<Member> members) throws IOException {
    Boolean firstLine = true;

    FileOutputStream persistentMemberData = new FileOutputStream("members.txt");
    OutputStreamWriter streamWriter = new OutputStreamWriter(persistentMemberData, "UTF-8");
    BufferedWriter memberWriter = new BufferedWriter(streamWriter);

    for (Member member : members) {
      String memberString = memberString(member, firstLine);
      firstLine = false;
      memberWriter.write(memberString);
    }

    memberWriter.close();
  }
  
  /**
   * Generates a list of member objects.
   *
   * @param memberList - List of member data strings.
   * @return - List of member objects.
   */
  private ArrayList<Member> memberObjectsList(ArrayList<String> memberList) {
    ArrayList<Member> members = new ArrayList<>();

    for (String memberLine : memberList) {
      Member member = createMemberObj(memberLine);
      members.add(member);
    }
    return members;
  }

  /**
   * Creates a member object from a string.
   *
   * @param memberDataString - A string of member data.
   * @return - A member object.
   */
  private Member createMemberObj(String memberData) {
    String[] memberDataArr = memberData.split(", ");

    Member member = new Member(memberDataArr[0], Long.parseLong(memberDataArr[1]));
    member.setId(memberDataArr[2]);

    String boatString = memberData.substring(memberData.indexOf(':') + 1, memberData.indexOf('}')).trim();
    if (boatString.length() > 0) { 
      member.addBoatList(memberBoatList(boatString)); 
    }
    return member;
  }

  /**
   * Creates a list of boat objects.
   *
   * @param memberBoatString - A string of a member's boats.
   * @return - A list of boat objects.
   */
  private ArrayList<Boat> memberBoatList(String memberBoatString) {
    String[] boats = memberBoatString.split(",");
    ArrayList<Boat> boatObjectsList = new ArrayList<>();
    
    for (String boat : boats) {
      boatObjectsList.add(createBoatObj(boat.trim()));
    }
    
    return boatObjectsList;
  }

  /**
   * Creates a boat object from a string.
   *
   * @param boatString - A string of boat data.
   * @return - A boat object.
   */
  private Boat createBoatObj(String boatString) {
    String[] splitString = boatString.split(" ");

    Boat boatObj = new Boat(splitString[0], splitString[1], Float.parseFloat(splitString[2]));
    
    return boatObj;
  }

  /**
   * Creates a string of member data.
   *
   * @param member - A member object.
   * @param firstLine - Tells if this is the first line to be written in the file.
   * @return - A string of member data.
   */
  private String memberString(Member member, Boolean firstLine) {
    ArrayList<String> boatStrings = new ArrayList<>();
    for (Boat boat : member.allBoats()) {
      boatStrings.add(boat.getName() + ' ' + boat.getType()  + ' ' + boat.getLength());
    }
    
    String memberString = "";
    if (!firstLine) { 
      memberString += "\n"; 
    }
    memberString += member.getName() + ", " + member.getPersonalNum() + ", " + member.getId() +  ", ";
    memberString += "{:" + String.join(", ", boatStrings) + '}';

    return memberString;
  }
}
