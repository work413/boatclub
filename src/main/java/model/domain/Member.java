package model.domain;

import java.util.ArrayList;

/**
 * Member class.
 */
public class Member {
  private String name;
  private Long personalNum;
  private String id;
  private ArrayList<Boat> boats = new ArrayList<>();

  public Member(String name, Long personalNum) {
    this.name = name;
    this.personalNum = personalNum;
  }

  protected void setName(String name) {
    this.name = name;
  }

  protected void setPersonalNum(Long num) {
    this.personalNum = num;
  }

  public void setId(String id) {
    this.id = id;
  }

  protected void addBoat(Boat newBoat) {
    boats.add(newBoat);
  }

  protected void deleteBoat(Boat boat) {
    boats.remove(boat);
  }

  public void addBoatList(ArrayList<Boat> boats) {
    this.boats = boats;
  }

  public String getName() {
    return name;
  }

  public Long getPersonalNum() {
    return personalNum;
  }

  public String getId() {
    return id;
  }

  public Boat getBoat(Integer index) {
    return boats.get(index);
  }

  public ArrayList<Boat> allBoats() {
    return boats;
  }
}
