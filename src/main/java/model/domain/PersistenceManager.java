package model.domain;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Manages the persistent data.
 */
public class PersistenceManager {
  private ArrayList<Member> members = new ArrayList<>();

  public void addSavedMembers(ArrayList<Member> savedMembers) {
    this.members = savedMembers;
  }

  public ArrayList<Member> allMembers() {
    return members;
  }
  
  /**
   * Creates a Member object and adds it to the members list.

   * @param name - The name of the member.
   * @param personalNum - The personalnumber of the member.
   */
  public void createNewMember(String name, Long personalNum) {
    Member member = new Member(name, personalNum);
    String id = generateUniqueId();
    member.setId(id);
    members.add(member);
  }

  /**
   * Creates a Boat object.

   * @param name - The boat's name.
   * @param type - The type.
   * @param length The length.
   * @return - A Boat object.
   */
  public Boat createNewBoat(String name, String type, Float length) {
    Boat newBoat = new Boat(name, type, length);

    return newBoat;
  }

  public void setMemberName(Member member, String name) {
    member.setName(name);
  }

  public void setMemberPersonalNum(Member member, Long personalNum) {
    member.setPersonalNum(personalNum);
  }

  public void setBoatName(Boat boat, String name) {
    boat.setName(name);
  }

  public void setBoatType(Boat boat, String type) {
    boat.setType(type);
  }

  public void setBoatLength(Boat boat, Float length) {
    boat.setLength(length);
  }

  public void setBoatToMember(Member owner, Boat newBoat) {
    owner.addBoat(newBoat);
  }

  /**
   * Checks if the boat's name already exists.

   * @param name - The boat's name.
   * @return - True if it is unique.
   */
  public boolean uniqueBoatName(String name) {
    for (Member member : members) {
      for (Boat boat : member.allBoats()) {
        if (name.equals(boat.getName())) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Deletes a Member from the members list if it exists.

   * @param idSearch - The ID of the member.
   * @return - True if the member was found and deleted.
   */
  public Boolean deleteMember(String idSearch) {
    Member member = getMember(idSearch);

    if (member != null) {
      members.remove(member);
      return true;
    } else {
      return false;
    }
  }

  public void deleteBoat(Member member, Boat boat) {
    member.deleteBoat(boat);
  }

  /**
   * Searches for a member. 
   *
   * @param idSearch - The member's ID.
   * @return - A member object, or null if the member couldn't be found.
   */
  public Member getMember(String idSearch) {
    for (Member memberObj : members) {
      if (memberObj.getId().equals(idSearch)) {
        return memberObj;
      }
    }
    return null;
  }

  /**
   * Generates a unique ID.
   *
   * @return - A string of the ID.
   */
  private String generateUniqueId() {
    String id;
    ArrayList<String> usedIds = new ArrayList<>();

    for (Member member : members) { 
      usedIds.add(member.getId()); 
    }
    do { 
      UUID randomCharacters = UUID.randomUUID(); 
      id = randomCharacters.toString().replaceAll("-", "").substring(0, 6);
    } while (usedIds.indexOf(id) >= 0);
      
    return id;
  }
}
