package model.domain;

/**
 * Boat class.
 */
public class Boat {
  private String type;
  private Float length;
  private String name;

  /**
   * Sets the properties of this object.
   *
   * @param name - The name of the boat.
   * @param type - The type of boat.
   * @param length - The length.
   */
  public Boat(String name, String type, Float length) {
    this.name = name;
    this.type = type;
    this.length = length;
  }

  protected void setType(String type) {
    this.type = type;
  }

  protected void setLength(Float length) {
    this.length = length;
  }

  protected void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public Float getLength() {
    return length;
  }

  public String getName() {
    return name;
  }
}
