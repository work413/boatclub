# BoatClub

**Usage explanaition:**

To use this application, first go to the App class in the controller package and run the main method. That method is responsible for calling the PersistentData class setting the language, as well making calls to the Register class. To select a language, simple change the instance of the user interface to either EnglishUi or SwedishUi. After being initiated by the App class, the Register object then calls the ConsoleUi class in the view package to display the main menu and communicate with the user through the console. Register then handles all the requests from the user's input.
When opening the main menu, the user is given the option to display a verbose list of all members, display a compact list of all members, show all the member options or quitting the application. When selecting to show all member options, the user is given the options to either add, show, update or delete a member. If the user chooses to update a member, he/she can choose to update the name, the personalnumber, information about a boat or to add a boat. Information about a boat that can be changed is: the name, the type or the length.
Before the call to view the menu is made, persistent data is loaded from txt-files and made into Member- and Boat-objects
These objects are the ones being changed and handled throughout the life of the application. This process happens in the PersistentData class. When the user wishes to quit the application, all the data in the objects are stored inside the txt-files to be used the next time the application is opened.

**Issues:** The application can't be exited from any other menu than the main one, neither is there an option to go back, instead the user is redirected back to the main menu after completing a request(for example; after succesfully adding a boat to a member). The application quits when trying to choose a boat that doesn't exist. There is no limit for how long a boat or a personalnumber can be.

**Assumptions:** For error handling, the application simply states that something went wrong, and redirects the user to the main menu. Also my application is persistent, eventhough it wasn't reqiured for a passing grade, simply because I want to practice storing data persistently. I assumed that there didn't need to be an option for deleting a boat. Regarding design, I'm aware that my class diagram shows a lot of methods. This is because I had some trouble with choosing only the most important ones. I tried choosing the ones that show a relation to another class or package, or is used by another class or package, besides the ones on a higher abtraction level.

**Model package:**

Inside the model package, I've put two folders; domain and persistence. The persistence sub-package, contains the PersistentData class, which handles loading and saving of persistent data. The domain sub-package contains the business logic. It consists of a Boat, Member and a PersistenceManager class. Objects from the first two classes contains the data that is stored persistently. The PersistenceManager class handles the CRUD features of the application as well as generating ID:s for the members and checking that the boat names are unique. The PersistenceManager class has a dependency relation to the Member and Boat classes. The PersistentData class has an association relation to the Member class and a dependency relation to the Boat class. The Member class has an association relation to the Boat class, since Boat objects are used as properties in the Member class.
_The model package does not depend on any other package._

**View package:**

Inside the view package, is the ConsoleUi interface. The classes EnglishUi and SwedishUi implements the interface. As mentioned before, this class is responsible for communicating with the console. It send messages to the user, and takes input. The interface and classes all depend on the Member and Boat classes from the model package since they print the objects' data. However, it does not change the objects, it simply print's the values of their properties. Besides that, the class does not depend on any other class.
_The view package depends on the Member class and the Boat class in the model package._

**Controller package:**

Inside the controller package, I've put two classes. The App class that only is used when opening and closing the application, and the Register class with more responsibilty. The Register class communicates both with the view package, and the the model package. However, it does not directly communicate with the PersistentData class. The App class is the one responsible for making calls to that class. Register uses the model's classes Boat and Member to handle request from the user interface and the PersistenceManager class to handle CRUD requests. Register has a dependency relation to these classes and an association relation to the ConsoleUi interface. The App class has a dependency relation to the Member class, PersistentData class and the view package since objects from those are used as variables or arguments inside the App's main method.
_The controller package depends on both the model and the view package._
